FROM openjdk:8-jre-alpine

COPY ${JAR_FILE} app.jar
COPY docker-entrypoint.sh docker-entrypoint.sh

RUN touch app.jar \
 && touch docker-entrypoint.sh

VOLUME ["/tmp"]

EXPOSE 8180 18180

ENTRYPOINT ["./docker-entrypoint.sh"]
