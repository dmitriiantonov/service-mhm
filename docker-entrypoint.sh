#!/bin/sh
set -v

exec java ${JAVA_OPTS} -Dspring.profiles.active=${PROFILE}  -Djava.security.egd=file:/dev/./urandom -jar app.jar