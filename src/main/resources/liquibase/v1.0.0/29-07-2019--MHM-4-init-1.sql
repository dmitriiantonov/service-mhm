

CREATE SEQUENCE IF NOT EXISTS customer_sequence START 1 INCREMENT 1;

CREATE TABLE IF NOT EXISTS customer
(
    id BIGINT NOT NULL DEFAULT nextval('customer_sequence'),
    phone VARCHAR(10) NOT NULL,
    first_name VARCHAR(50),
    last_name VARCHAR(50),
    patronymic  VARCHAR(50),
    created_date  TIMESTAMP    NOT NULL,
    modified_date TIMESTAMP    NOT NULL,
    CONSTRAINT pk_customer PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS customer_role_sequence START 1 INCREMENT 1;

CREATE TABLE IF NOT EXISTS customer_role
(
    id            BIGINT      NOT NULL DEFAULT nextval('customer_role_sequence'),
    name          VARCHAR(50) NOT NULL,
    customer_id   BIGINT,
    created_date  TIMESTAMP   NOT NULL,
    modified_date TIMESTAMP   NOT NULL,
    CONSTRAINT pk_customer_role PRIMARY KEY (id),
    CONSTRAINT fk_customer_role_customer FOREIGN KEY (customer_id) REFERENCES customer(id)
);

CREATE SEQUENCE IF NOT EXISTS sending_code_sequence START 1 INCREMENT 1;

CREATE TABLE IF NOT EXISTS sending_code
(
    id BIGINT NOT NULL DEFAULT nextval('sending_code_sequence'),
    customer_id BIGINT,
    code VARCHAR(4) NOT NULL,
    confirmed BOOLEAN NOT NULL,
    number_of_attempts INT NOT NULL DEFAULT 0,
    created_date  TIMESTAMP    NOT NULL,
    modified_date TIMESTAMP    NOT NULL,
    CONSTRAINT pk_sending_code PRIMARY KEY (id),
    CONSTRAINT fk_sending_code_customer FOREIGN KEY (customer_id) REFERENCES customer(id)
);
