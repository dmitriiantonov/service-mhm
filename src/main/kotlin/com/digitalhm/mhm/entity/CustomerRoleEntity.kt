package com.digitalhm.mhm.entity

import com.digitalhm.mhm.enum.ApplicationUserRole
import com.digitalhm.mhm.security.model.ApplicationGrantedAuthority
import javax.persistence.*

@Entity
@Table(name = "customer_role")
class CustomerRoleEntity(
        @Column(name = "name", updatable = false, nullable = false)
        @Enumerated(EnumType.STRING)
        val name: ApplicationUserRole,

        @ManyToOne
        @JoinColumn(name = "customer_id", nullable = false, updatable = false)
        var customer: CustomerEntity
) : AuditableEntity() {

    companion object {
        const val GENERATOR_NAME: String = "CUSTOMER_SEQUENCE_GENERATOR"
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = "customer_role_sequence", initialValue = 1, allocationSize = 1)
    var id: Long? = null

    fun grantedAuthority(): ApplicationGrantedAuthority {
        return ApplicationGrantedAuthority(name.name)
    }
}
