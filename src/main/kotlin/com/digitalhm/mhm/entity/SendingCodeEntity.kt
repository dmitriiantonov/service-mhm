package com.digitalhm.mhm.entity

import javax.persistence.*

@Entity
@Table(name = "sending_code")
class SendingCodeEntity(
        @Column(name = "code", nullable = false, updatable = false)
        val code: String,

        @Column(name = "confirmed", nullable = false)
        var confirmed: Boolean = false,

        @ManyToOne
        @JoinColumn(name = "customer_id", nullable = false, updatable = false)
        var customer: CustomerEntity? = null
) : AuditableEntity() {

    companion object {
        const val GENERATOR_NAME: String = "SENDING_CODE_SEQUENCE_GENERATOR"
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = "sending_code_sequence", initialValue = 1, allocationSize = 1)
    var id: Long? = null

    @Column(name = "number_of_attempts", nullable = false)
    var numberOfAttempts: Int = 0

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SendingCodeEntity

        if (code != other.code) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = code.hashCode()
        result = 31 * result + (id?.hashCode() ?: 0)
        return result
    }


}
