package com.digitalhm.mhm.entity

import javax.persistence.*

@Entity
@Table(name = "customer")
class CustomerEntity(
	var phone: String,
	var firstName: String? = null,
	var lastName: String? = null,
	var patronymic: String? = null
) : AuditableEntity() {

	companion object {
		const val GENERATOR_NAME: String = "CUSTOMER_SEQUENCE_GENERATOR"
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
	@SequenceGenerator(name = GENERATOR_NAME, sequenceName = "customer_sequence", initialValue = 1, allocationSize = 1)
	var id: Long? = null

	@OneToMany(mappedBy = "customer", cascade = [CascadeType.ALL])
	var sendingCodes: MutableSet<SendingCodeEntity> = mutableSetOf()

	@OneToMany(mappedBy = "customer", cascade = [CascadeType.ALL])
	var roles: MutableSet<CustomerRoleEntity> = mutableSetOf()
}
