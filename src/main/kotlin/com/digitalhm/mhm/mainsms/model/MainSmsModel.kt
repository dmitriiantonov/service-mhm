package com.digitalhm.mhm.mainsms.model

import com.fasterxml.jackson.annotation.JsonProperty

class MainSmsModel {
	var status: String? = null
	var recipients: List<String> = mutableListOf()
	var parts: Long? = null
	var count: Long? = null
	var price: String? = null
	var balance: String? = null
	@JsonProperty("message_id")
	var messageId: List<Long>? = mutableListOf()
	var test: Long? = null
}
