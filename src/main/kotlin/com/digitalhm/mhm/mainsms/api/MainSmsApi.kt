package com.digitalhm.mhm.mainsms.api

import com.digitalhm.mhm.mainsms.model.MainSmsModel
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(url = "\${external-service.main-sms.host}", value = "main-sms")
interface MainSmsApi {

	@GetMapping("/send")
	fun send(
		@RequestParam("project") project: String,
		@RequestParam("sender") sender: String,
		@RequestParam("apikey") apiKey: String,
		@RequestParam("message") message: String,
		@RequestParam("recipients") phone: String
	): MainSmsModel
}
