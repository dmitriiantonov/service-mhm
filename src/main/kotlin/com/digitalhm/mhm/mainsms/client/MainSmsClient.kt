package com.digitalhm.mhm.mainsms.client

import com.digitalhm.mhm.properties.MainSmsProperties
import com.digitalhm.mhm.mainsms.api.MainSmsApi
import com.digitalhm.mhm.mainsms.model.MainSmsModel
import org.springframework.stereotype.Component

@Component
class MainSmsClient(
        private val mainSmsApi: MainSmsApi,
        private val properties: MainSmsProperties
) {
    fun send(message: String, phone: String): MainSmsModel {
        return mainSmsApi.send(properties.project, properties.sender, properties.apiKey, message, phone)
    }
}
