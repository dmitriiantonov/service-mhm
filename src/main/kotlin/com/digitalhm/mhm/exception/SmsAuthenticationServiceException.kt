package com.digitalhm.mhm.exception

import java.lang.RuntimeException

class SmsAuthenticationServiceException(
        message: String? = null,
        cause: Throwable? = null
) : RuntimeException(message, cause)
