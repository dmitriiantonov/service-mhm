package com.digitalhm.mhm.rest

import javax.validation.groups.Default

interface SendSmsGroup: Default

interface CodeVerificationGroup: Default
