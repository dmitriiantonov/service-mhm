package com.digitalhm.mhm.rest.dto

import com.digitalhm.mhm.rest.CodeVerificationGroup
import com.digitalhm.mhm.rest.SendSmsGroup
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

class SmsAuthenticationDTO(
	@Pattern(regexp = "9[0-9]{9}")
	@NotNull(groups = [SendSmsGroup::class, CodeVerificationGroup::class])
	var phone: String? = null,
	@NotNull(groups = [CodeVerificationGroup::class])
	var code: String? = null,
	var token: String? = null
)
