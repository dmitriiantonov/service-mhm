package com.digitalhm.mhm.rest.dto

abstract class AbstractResponse(
	val success: Boolean
)

class SuccessResponse<T>(val data: T): AbstractResponse(true)

class FailedResponse(errors: List<Error>): AbstractResponse(false)

data class Error(
	val message: String,
	val code: Int
)
