package com.digitalhm.mhm.rest.controller

import com.digitalhm.mhm.rest.CodeVerificationGroup
import com.digitalhm.mhm.rest.SendSmsGroup
import com.digitalhm.mhm.rest.dto.SmsAuthenticationDTO
import com.digitalhm.mhm.rest.dto.SuccessResponse
import com.digitalhm.mhm.service.SmsAuthenticationService
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/auth")
class SmsAuthenticationController(
		private val smsAuthenticationService: SmsAuthenticationService
) {

	@PostMapping("/send-sms")
	fun sendSms(@RequestBody @Validated(SendSmsGroup::class) authentication: SmsAuthenticationDTO): SuccessResponse<SmsAuthenticationDTO> {
		smsAuthenticationService.sendMessage(authentication.phone!!)
		return SuccessResponse(SmsAuthenticationDTO())
	}

	@PostMapping("send-code")
	fun sendCode(@RequestBody @Validated(CodeVerificationGroup::class) authentication: SmsAuthenticationDTO): SuccessResponse<SmsAuthenticationDTO> {
		if (authentication.code != null && authentication.phone != null) {
			val generatedToken = smsAuthenticationService.createJwtToken(authentication.phone!!, authentication.code!!)
			return SuccessResponse(SmsAuthenticationDTO().apply {
				phone = authentication.phone
				code = authentication.code
				token = generatedToken
			})
		}
		return SuccessResponse(SmsAuthenticationDTO())
	}
}
