package com.digitalhm.mhm.service

import com.digitalhm.mhm.entity.CustomerEntity
import com.digitalhm.mhm.entity.SendingCodeEntity
import com.digitalhm.mhm.exception.SendingCodeServiceException
import com.digitalhm.mhm.repository.BaseJpaRepository
import com.digitalhm.mhm.repository.SendingCodeRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Propagation.*
import org.springframework.transaction.annotation.Transactional

@Service
class SendingCodeService(
        private val sendingCodeRepository: SendingCodeRepository
) : CrudService<SendingCodeEntity, Long>() {

    @Transactional(propagation = REQUIRES_NEW)
    fun verifyCode(customer: CustomerEntity, code: String): Boolean {
        val foundedSendingCode = findSendingCode(customer);
        return if (code == foundedSendingCode.code) {
            foundedSendingCode.confirmed = true
            save(foundedSendingCode)
            true
        } else {
            foundedSendingCode.numberOfAttempts += 1
            save(foundedSendingCode)
            false
        }
    }

    private fun findSendingCode(customer: CustomerEntity): SendingCodeEntity {
        return (customer.sendingCodes
                .filter { !it.confirmed }
                .maxBy { it.createdDate }
                ?: throw SendingCodeServiceException("Cannot find sending code for customer with phone: ${customer.phone}"))
    }

    @Transactional
    fun addSendingCode(customer: CustomerEntity, code: String): CustomerEntity {
        val sendingCode = SendingCodeEntity(code, false, customer)
        customer.sendingCodes.add(sendingCode)
        save(sendingCode)
        return customer
    }

    override val repository: BaseJpaRepository<SendingCodeEntity, Long>
        get() = sendingCodeRepository
}
