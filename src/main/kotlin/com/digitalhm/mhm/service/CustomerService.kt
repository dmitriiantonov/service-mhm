package com.digitalhm.mhm.service

import com.digitalhm.mhm.entity.CustomerEntity
import com.digitalhm.mhm.entity.SendingCodeEntity
import com.digitalhm.mhm.repository.CustomerRepository
import com.digitalhm.mhm.repository.BaseJpaRepository
import org.springframework.stereotype.Service

@Service
class CustomerService(
	private val customerRepository: CustomerRepository
): CrudService<CustomerEntity, Long>() {

	fun findByPhone(phone: String): CustomerEntity? = customerRepository.findByPhone(phone)

	fun createCustomer(phone: String, code: String): CustomerEntity {
		val customer = CustomerEntity(phone).apply {
			sendingCodes.add(SendingCodeEntity(code, false, this))
		}
		return save(customer)
	}

	override val repository: BaseJpaRepository<CustomerEntity, Long>
		get() = customerRepository
}
