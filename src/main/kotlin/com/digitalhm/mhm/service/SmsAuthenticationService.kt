package com.digitalhm.mhm.service

import com.digitalhm.mhm.security.model.TokenPayload
import com.digitalhm.mhm.entity.CustomerEntity
import com.digitalhm.mhm.entity.CustomerRoleEntity
import com.digitalhm.mhm.entity.SendingCodeEntity
import com.digitalhm.mhm.enum.ApplicationUserRole
import com.digitalhm.mhm.exception.SmsAuthenticationServiceException
import com.digitalhm.mhm.mainsms.client.MainSmsClient
import org.apache.commons.lang3.RandomUtils
import org.springframework.stereotype.Service

@Service
class SmsAuthenticationService(
        private val customerService: CustomerService,
        private val sendingCodeService: SendingCodeService,
        private val mainSmsClient: MainSmsClient,
        private val tokenService: JwtTokenService
) {

    fun sendMessage(phone: String) {
        val generatedCode = generateCode()
        customerService.findByPhone(phone)?.let { customer ->
            sendingCodeService.addSendingCode(customer, generatedCode)
        } ?: customerService.createCustomer(phone, generatedCode)
        mainSmsClient.send(message = generatedCode, phone = phone)
    }

    private fun generateCode(): String {
        return RandomUtils.nextInt(1, 9999).toString()
    }

    fun createJwtToken(phone: String, code: String): String {
		val foundedCustomer = customerService.findByPhone(phone)
				?: throw SmsAuthenticationServiceException("Cannot find customer with phone: $phone")

		return if (sendingCodeService.verifyCode(foundedCustomer, code)) {
			tokenService.generateJwtToken(TokenPayload(phone))
		} else {
			throw SmsAuthenticationServiceException("Cannot authenticate customer with phone: $phone and code: $code")
		}
    }
}
