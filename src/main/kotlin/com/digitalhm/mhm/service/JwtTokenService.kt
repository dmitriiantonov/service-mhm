package com.digitalhm.mhm.service

import com.digitalhm.mhm.properties.Properties
import com.digitalhm.mhm.security.model.TokenPayload
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.jwt.JwtHelper
import org.springframework.security.jwt.crypto.sign.MacSigner
import org.springframework.stereotype.Service

@Service
class JwtTokenService(
        private val mapper: ObjectMapper,
        private val properties: Properties
) {

    fun generateJwtToken(payload: TokenPayload): String {
        val token = mapper.writeValueAsString(payload)
        return JwtHelper.encode(token, MacSigner(properties.secretKey)).encoded
    }
}
