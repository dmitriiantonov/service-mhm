package com.digitalhm.mhm.service

import com.digitalhm.mhm.entity.AuditableEntity
import com.digitalhm.mhm.repository.BaseJpaRepository
import java.io.Serializable

abstract class CrudService<T: AuditableEntity, ID: Serializable> {

    fun findAll() = repository.findAll()

    fun findAllById(ids: Iterable<ID>) = repository.findAllById(ids)

    fun findById(id: ID) = repository.findById(id)

    fun save(entity: T) = repository.save(entity)

    fun saveAll(entities: Iterable<T>) = repository.saveAll(entities)

    fun delete(entity: T) = repository.delete(entity)

    fun deleteAll(entities: Iterable<T>) = repository.deleteAll(entities)

    abstract val repository: BaseJpaRepository<T, ID>
}
