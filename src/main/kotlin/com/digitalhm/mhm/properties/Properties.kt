package com.digitalhm.mhm.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotNull

@Validated
@ConfigurationProperties(prefix = "jwt")
class Properties {
    @NotNull
    lateinit var secretKey: String
}
