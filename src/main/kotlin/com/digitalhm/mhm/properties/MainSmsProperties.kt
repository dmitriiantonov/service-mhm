package com.digitalhm.mhm.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotNull

@Validated
@ConfigurationProperties(prefix = "external-service.main-sms")
class MainSmsProperties {
	@NotNull
	lateinit var host: String
	@NotNull
	lateinit var project: String
	@NotNull
	lateinit var sender: String
	@NotNull
	lateinit var apiKey: String
}
