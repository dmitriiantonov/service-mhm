package com.digitalhm.mhm

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableFeignClients
@SpringBootApplication
class MobileHomeManagementApplication

fun main(args: Array<String>) {
    runApplication<MobileHomeManagementApplication>(*args)
}
