package com.digitalhm.mhm.security.service

import com.digitalhm.mhm.security.model.ApplicationUserDetails
import com.digitalhm.mhm.service.CustomerService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class ApplicationUserDetailsService(
        private val customerService: CustomerService
) : UserDetailsService {

    @Transactional
    override fun loadUserByUsername(phone: String): UserDetails {
        val customer = customerService.findByPhone(phone)
                ?: throw UsernameNotFoundException("Customer with phone: $phone not found")
		val roles = customer.roles
				.map { role -> role.grantedAuthority() }
				.toMutableList()
        return ApplicationUserDetails(customer.phone, roles)
    }
}
