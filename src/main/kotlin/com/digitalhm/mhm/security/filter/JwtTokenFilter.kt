package com.digitalhm.mhm.security.filter

import com.digitalhm.mhm.security.model.TokenPayload
import com.digitalhm.mhm.properties.Properties
import com.digitalhm.mhm.security.service.ApplicationUserDetailsService
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.jwt.JwtHelper
import org.springframework.security.jwt.crypto.sign.MacSigner
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
class JwtTokenFilter(
        private val properties: Properties,
        private val mapper: ObjectMapper,
        private val userDetailsService: ApplicationUserDetailsService
) : OncePerRequestFilter() {

    companion object {
        private const val AUTH_TOKEN: String = "Api-Token"
    }

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val token = request.getHeader(AUTH_TOKEN)
                ?: return filterChain.doFilter(request, response)

        val jwt = JwtHelper.decodeAndVerify(token, MacSigner(properties.secretKey))

        val payload: TokenPayload = mapper.readValue(jwt.claims)
        SecurityContextHolder.getContext().authentication = getAuthentication(payload.phone)
        filterChain.doFilter(request, response)
    }

    fun getAuthentication(phone: String): Authentication {
        val userDetails = userDetailsService.loadUserByUsername(phone)
        return UsernamePasswordAuthenticationToken(userDetails, "", userDetails.authorities)
    }
}
