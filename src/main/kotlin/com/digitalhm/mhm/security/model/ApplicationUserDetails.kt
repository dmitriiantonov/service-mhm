package com.digitalhm.mhm.security.model

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class ApplicationUserDetails(
        private val phone: String,
        private val grantedAuthorities: MutableList<ApplicationGrantedAuthority>
) : UserDetails {

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = grantedAuthorities

    override fun isEnabled(): Boolean = true

    override fun getUsername(): String = phone

    override fun isCredentialsNonExpired(): Boolean = true

    override fun getPassword(): String = ""

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true
}
