package com.digitalhm.mhm.security.model

import org.springframework.security.core.GrantedAuthority

class ApplicationGrantedAuthority(
	private val authority: String
) : GrantedAuthority {
	override fun getAuthority(): String = authority

}
