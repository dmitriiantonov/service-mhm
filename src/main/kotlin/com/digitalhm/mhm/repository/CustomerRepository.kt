package com.digitalhm.mhm.repository

import com.digitalhm.mhm.entity.CustomerEntity

interface CustomerRepository : BaseJpaRepository<CustomerEntity, Long> {
	fun findByPhone(phone: String): CustomerEntity?
}
