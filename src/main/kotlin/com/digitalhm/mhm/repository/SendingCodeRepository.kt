package com.digitalhm.mhm.repository

import com.digitalhm.mhm.entity.SendingCodeEntity

interface SendingCodeRepository : BaseJpaRepository<SendingCodeEntity, Long>
