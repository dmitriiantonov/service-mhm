package com.digitalhm.mhm.repository

import com.digitalhm.mhm.entity.AuditableEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean
import java.io.Serializable

@NoRepositoryBean
interface BaseJpaRepository<T: AuditableEntity, ID : Serializable> : CrudRepository<T, ID>
