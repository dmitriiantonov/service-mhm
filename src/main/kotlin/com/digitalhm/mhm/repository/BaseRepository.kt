package com.digitalhm.mhm.repository

import com.digitalhm.mhm.entity.AuditableEntity
import org.springframework.data.repository.CrudRepository
import java.io.Serializable

interface BaseRepository<T: AuditableEntity, ID : Serializable> : CrudRepository<T, ID>
