package com.digitalhm.mhm.enum

enum class ApplicationUserRole {
	USER, VERIFIED_USER
}
